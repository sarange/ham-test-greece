import requests

url = 'https://www.artinoi.gr/cgi-bin/forumV2/sv.pl'


for sv_sy in ['sy', 'sv']:
    for i in range(1,13 if sv_sy == 'sy' else 25):
        num = str(i).zfill(2) if sv_sy == 'sy' else str(i+13)
        data = {
            'action': 'save',
            'lesson': num,
            'transfer': '',
            'Submit': ''
        }

        cont = requests.post(url, data=data).content

        with open(f'{sv_sy}_{str(i).zfill(2)}.html', 'wb') as f:
            f.write(cont)