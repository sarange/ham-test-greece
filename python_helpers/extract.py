from bs4 import BeautifulSoup
import re
import json

regex_correct = re.compile(r'ok\.gif.*?(Α|Β|Γ|Δ)\.')
regex_answers = re.compile(r'(?:Α|Β|Γ|Δ)\. (.*?)</font>')
regex_title = re.compile(r'<b>\d+\. (.*?)</b>')
regex_img = re.compile(r'<img src="../../sv/(.*?)"')

correct_table = {
    'Α': 1,
    'Β': 2,
    'Γ': 3,
    'Δ': 4
}
for sv_sy in ['sy', 'sv']:

    result = []
    seen_titles = []
    number = 1
    
    for i in range(1,13 if sv_sy == 'sy' else 25):

        with open(f'{sv_sy}_{str(i).zfill(2)}.html', 'rb') as f:
            page = f.read()

        soup = BeautifulSoup(page, 'html.parser')
        tables = soup.find_all('table', class_='small')
        for table in tables:
            if 'Δεν δώσατε καμία απάντηση' in table.text:
                table = str(table)
                title = regex_title.findall(table)[0]
                if title in seen_titles:
                    continue
                else:
                    seen_titles.append(title)
                correct = regex_correct.findall(table)[0]
                correct_num = correct_table[correct]
                answers = regex_answers.findall(table)
                if len(answers) == 4:
                    tmp_result = {
                        'number': number,
                        'title': title,
                        'answer_1': answers[0],
                        'answer_2': answers[1],
                        'answer_3': answers[2],
                        'answer_4': answers[3],
                        'correct': correct_num
                    }
                elif len(answers) == 3:
                    tmp_result = {
                        'number': number,
                        'title': title,
                        'answer_1': answers[0],
                        'answer_2': answers[1],
                        'answer_3': answers[2],
                        'correct': correct_num
                    }
                elif len(answers) == 2:
                    tmp_result = {
                        'number': number,
                        'title': title,
                        'answer_1': answers[0],
                        'answer_2': answers[1],
                        'correct': correct_num
                    }
                if 'jpg' in table:
                    img = regex_img.findall(table)[0]
                    tmp_result['img'] = img.replace('/', '_')
                result.append(tmp_result)
                number += 1

    with open(f'{sv_sy}.json', 'wb') as f:
        f.write(json.dumps(result, ensure_ascii=False, indent=4).encode('utf8'))