import requests
import json

for sv_sy in ['sy', 'sv']:
    with open(f'{sv_sy}.json', 'r') as f:
        _json = json.load(f)

    for question in _json:
        if 'img' in question.keys():
            img = requests.get('https://www.artinoi.gr/sv/' + question['img'].replace('_', '/')).content
            with open(question['img'], 'wb') as f:
                f.write(img)