package gr.app.hamtest

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class QuestionAdapter(private var questionList: List<Map<String, Any>>, private val context: Context): RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>() {
    inner class QuestionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val question: TextView = view.findViewById(R.id.question)
        val answer1: RadioButton = view.findViewById(R.id.answer1)
        val answer2: RadioButton = view.findViewById(R.id.answer2)
        val answer3: RadioButton = view.findViewById(R.id.answer3)
        val answer4: RadioButton = view.findViewById(R.id.answer4)
        val image: ImageView = view.findViewById(R.id.imageView)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_question, parent, false)
        return QuestionViewHolder(itemView)
    }
    @SuppressLint("DiscouragedApi")
    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        // Set text and visibility
        val question = questionList[position]
        holder.question.text = question["title"].toString()
        holder.answer1.text = question["answer_1"].toString()
        holder.answer2.text = question["answer_2"].toString()
        if (question.containsKey("answer_4")) {
            holder.answer3.text = question["answer_3"].toString()
            holder.answer3.visibility = View.VISIBLE
            holder.answer4.text = question["answer_4"].toString()
            holder.answer4.visibility = View.VISIBLE
        } else if (question.containsKey("answer_3")) {
            holder.answer3.text = question["answer_3"].toString()
            holder.answer3.visibility = View.VISIBLE
            holder.answer4.visibility = View.GONE
        } else {
            holder.answer3.visibility = View.GONE
            holder.answer4.visibility = View.GONE
        }
        holder.answer1.isClickable = false
        holder.answer2.isClickable = false
        holder.answer3.isClickable = false
        holder.answer4.isClickable = false

        if (question.containsKey("img")) {
            holder.image.visibility = View.VISIBLE
            val imgName = question["img"].toString()
            val resourceId = context.resources.getIdentifier(imgName, "drawable", context.packageName)
            holder.image.setImageResource(resourceId)
        } else {
            holder.image.visibility = View.GONE
        }

        // Set colors
        val defaultColor = ContextCompat.getColor(context, R.color.white)
        val correctColor = ContextCompat.getColor(context, R.color.correctColor)
        val wrongColor = ContextCompat.getColor(context, R.color.wrongColor)
        holder.answer1.setTextColor(defaultColor)
        holder.answer2.setTextColor(defaultColor)
        holder.answer3.setTextColor(defaultColor)
        holder.answer4.setTextColor(defaultColor)

        when(question["correct"]) {
            1.0 -> {
                holder.answer1.isChecked = true
                holder.answer1.setTextColor(correctColor)
            }
            2.0 -> {
                holder.answer2.isChecked = true
                holder.answer2.setTextColor(correctColor)
            }
            3.0 -> {
                holder.answer3.isChecked = true
                holder.answer3.setTextColor(correctColor)
            }
            4.0 -> {
                holder.answer4.isChecked = true
                holder.answer4.setTextColor(correctColor)
            }
        }

        if (question["correct"] != question["answer"]) {
            when(question["answer"]) {
                1.0 -> {
                    holder.answer1.isChecked = true
                    holder.answer1.setTextColor(wrongColor)
                }
                2.0 -> {
                    holder.answer2.isChecked = true
                    holder.answer2.setTextColor(wrongColor)
                }
                3.0 -> {
                    holder.answer3.isChecked = true
                    holder.answer3.setTextColor(wrongColor)
                }
                4.0 -> {
                    holder.answer4.isChecked = true
                    holder.answer4.setTextColor(wrongColor)
                }
            }
        }
    }
    override fun getItemCount(): Int {
        return questionList.size
    }
}