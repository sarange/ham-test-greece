package gr.app.hamtest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val svClick = findViewById<ImageButton>(R.id.sv)
        svClick.setOnClickListener {
            val questionCount = findViewById<EditText>(R.id.questionCount)
            if (questionCount.text.toString().toInt() > 717) {
                questionCount.setText("717")
                val toast = Toast.makeText(applicationContext, "Max question count for SV is 717", Toast.LENGTH_SHORT)
                toast.show()
            } else if (questionCount.text.toString().toInt() < 1) {
                questionCount.setText("1")
                val toast = Toast.makeText(applicationContext, "You must select at least 1 question", Toast.LENGTH_SHORT)
                toast.show()
            } else {
                val intent = Intent(this, QuizActivity::class.java)
                intent.putExtra("quiz", "sv")
                intent.putExtra("questionCount", questionCount.text.toString())
                startActivity(intent)
            }
        }

        val syClick = findViewById<ImageButton>(R.id.sy)
        syClick.setOnClickListener {
            val questionCount = findViewById<EditText>(R.id.questionCount)
            if (questionCount.text.toString().toInt() > 320) {
                questionCount.setText("320")
                val toast = Toast.makeText(applicationContext, "Max question count for SY is 320", Toast.LENGTH_SHORT)
                toast.show()
            } else if (questionCount.text.toString().toInt() < 1) {
                questionCount.setText("1")
                val toast = Toast.makeText(applicationContext, "You must select at least 1 question", Toast.LENGTH_SHORT)
                toast.show()
            } else {
                val intent = Intent(this, QuizActivity::class.java)
                intent.putExtra("quiz", "sy")
                intent.putExtra("questionCount", questionCount.text.toString())
                startActivity(intent)
            }
        }

    }
}