package gr.app.hamtest

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlin.random.Random


class QuizActivity: AppCompatActivity() {

    private lateinit var questionNumbers: MutableList<Int>
    private lateinit var jsonList: List<MutableMap<String, Any>>
    private lateinit var currentJsonQuestion: MutableMap<String, Any>
    private var answeredList: MutableList<Map<String, Any>> = mutableListOf(mapOf())
    private val gson = Gson()
    private var currentQuestion = 0
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        // Which test am I on?
        val extras = intent.extras
        val quiz = extras!!.getString("quiz")
        val questionCount = extras.getString("questionCount")

        // Create button listeners
        val nextButton = findViewById<Button>(R.id.button_ok)
        nextButton.setOnClickListener {
            nextQuestion()
        }

        val backButton = findViewById<Button>(R.id.button_back)
        backButton.setOnClickListener {
            previousQuestion()
        }

//        val skipButton = findViewById<Button>(R.id.button_skip)
//        skipButton.setOnClickListener {
//            skipQuestion()
//        }

        if (quiz != null && questionCount != null) {
            questionNumbers = getQuestionNumbers(quiz, questionCount.toInt())
        } else {
            throw Exception("There was no quiz passed in the activity")
        }

        val resourceId = if (quiz == "sv") {
            R.raw.sv
        } else {
            R.raw.sy
        }
        val jsonString = resources.openRawResource(resourceId).bufferedReader().use {
            it.readText()
        }

        val listType = object : TypeToken<List<Map<String, Any>>>() {}.type
        jsonList = gson.fromJson(jsonString, listType)

        displayQuestion()
    }

    private fun getQuestionNumbers(quiz: String, count: Int): MutableList<Int> {
        val maxNum = if (quiz == "sv") {
            717
        } else {
            320
        }
        val random = Random(System.currentTimeMillis())
        val result = mutableListOf<Int>()

        while (result.size < count) {
            val nextNumber = random.nextInt(1, maxNum + 1)
            if (!result.contains(nextNumber)) {
                result.add(nextNumber)
            }
        }

        return result
    }

    private fun nextQuestion() {
        val answer1 = findViewById<RadioButton>(R.id.answer1)
        val answer2 = findViewById<RadioButton>(R.id.answer2)
        val answer3 = findViewById<RadioButton>(R.id.answer3)
        val answer4 = findViewById<RadioButton>(R.id.answer4)
        currentJsonQuestion["answer"] = if (answer1.isChecked) {
            1
        } else if (answer2.isChecked) {
            2
        } else if (answer3.isChecked) {
            3
        } else if (answer4.isChecked) {
            4
        } else {
            val toast = Toast.makeText(
                applicationContext,
                "You have to select an option",
                Toast.LENGTH_SHORT
            )
            toast.show()
            return
        }
        answeredList.add(currentQuestion, currentJsonQuestion)
        if (currentQuestion + 1 == questionNumbers.size) {
            answeredList.removeLast()
            displayResults()
        } else {
            currentQuestion += 1
            displayQuestion()
        }
    }

    private fun previousQuestion() {
        currentQuestion -= 1
        displayQuestion()
    }

    @SuppressLint("DiscouragedApi")
    private fun displayQuestion() {

        val num = questionNumbers[currentQuestion]

        val question = findViewById<TextView>(R.id.question)
        val radioGroup = findViewById<RadioGroup>(R.id.answerGroup)
        val answer1 = findViewById<RadioButton>(R.id.answer1)
        val answer2 = findViewById<RadioButton>(R.id.answer2)
        val answer3 = findViewById<RadioButton>(R.id.answer3)
        val answer4 = findViewById<RadioButton>(R.id.answer4)
        val image = findViewById<ImageView>(R.id.imageView)
        val completion = findViewById<TextView>(R.id.completion)
        val back = findViewById<Button>(R.id.button_back)

        back.visibility = if (currentQuestion == 0) {
            View.GONE
        } else {
            View.VISIBLE
        }

        radioGroup.clearCheck()
        currentJsonQuestion = jsonList[num]

        if (currentJsonQuestion.containsKey("answer")) {
            when(currentJsonQuestion["answer"]) {
                1 -> answer1.isChecked = true
                2 -> answer2.isChecked = true
                3 -> answer3.isChecked = true
                4 -> answer4.isChecked = true
            }
        }
        question.text = currentJsonQuestion["title"].toString()
        answer1.text = currentJsonQuestion["answer_1"].toString()
        answer2.text = currentJsonQuestion["answer_2"].toString()
        if (currentJsonQuestion.containsKey("answer_4")) {
            answer3.text = currentJsonQuestion["answer_3"].toString()
            answer3.visibility = View.VISIBLE
            answer4.text = currentJsonQuestion["answer_4"].toString()
            answer4.visibility = View.VISIBLE
        } else if (currentJsonQuestion.containsKey("answer_3")) {
            answer3.text = currentJsonQuestion["answer_3"].toString()
            answer3.visibility = View.VISIBLE
            answer4.visibility = View.INVISIBLE
        } else {
            answer3.visibility = View.INVISIBLE
            answer4.visibility = View.INVISIBLE
        }

        if (currentJsonQuestion.containsKey("img")) {
            image.visibility = View.VISIBLE
            val imgName = currentJsonQuestion["img"].toString()
            val resourceId = resources.getIdentifier(imgName, "drawable", packageName)
            image.setImageResource(resourceId)
        } else {
            image.visibility = View.GONE
        }

        completion.text = getString(R.string.completion, currentQuestion + 1, questionNumbers.size)

    }

    private fun displayResults() {
        val intent = Intent(this, ResultsActivity::class.java)
        intent.putExtra("answeredList", gson.toJson(answeredList))
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit the quiz", Toast.LENGTH_SHORT).show()
        Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}
