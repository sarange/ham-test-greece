package gr.app.hamtest

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ResultsActivity: AppCompatActivity() {

    private var answeredList: List<Map<String, Any>> = mutableListOf(mapOf())
    private val gson = Gson()
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)

        val extras = intent.extras
        val answeredListString = extras!!.getString("answeredList")

        val listType = object : TypeToken<List<Map<String, Any>>>() {}.type
        answeredList = gson.fromJson(answeredListString, listType)

        val resultCount = findViewById<TextView>(R.id.resultCount)
        val correct = countResults()
        val passFail = if (correct / answeredList.size >= 0.8) {
            resultCount.setTextColor(ContextCompat.getColor(applicationContext, R.color.correctColor))
            "Pass"
        } else {
            resultCount.setTextColor(ContextCompat.getColor(applicationContext, R.color.wrongColor))
            "Fail"
        }

        resultCount.text = "$passFail - " +
                "${kotlin.math.floor((correct.toDouble() / answeredList.size.toDouble()) * 100).toInt()}% " +
                "(${correct}/${answeredList.size})"

        val recyclerView = findViewById<RecyclerView>(R.id.resultsView)
        val questionAdapter = QuestionAdapter(answeredList, applicationContext)
        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = questionAdapter
        questionAdapter.notifyItemRangeInserted(0, answeredList.size)

    }

    private fun countResults(): Int {
        var correct = 0
        for (answer in answeredList) {
            correct = if (answer["correct"] == answer["answer"]) {
                correct + 1
            } else {
                correct
            }
        }
        return correct
    }
}
