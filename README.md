# HAM Test - Greece

I just wanted an app to study for the HAM test, so here it is. Hope it helps.

[Compiled apk](https://gitlab.com/sarange/ham-test-greece/-/raw/master/app/release/app-release.apk)

## Credits
* https://www.artinoi.gr/cgi-bin/forumV2/sv.pl - Big thanks for the questions
* https://www.berovalis.gr/images/stories/PDF/SY.pdf
* https://www.berovalis.gr/images/stories/PDF/SV.pdf